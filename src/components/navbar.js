import React from "react";

function navbar({navbar}){
    const input=<nav class="navbar navbar-expand-lg navbar-light bg-light" data-testid="navbar" style={{
        margin: '10px',
      }}>
    <a class="navbar-brand" href="/#">Navbsdaar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="/#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" data-testid="navbar-button">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="/#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/#">Featuress</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/#">Pricings</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="/#" id="navbarDropdownMenuLink" data-toggle="dropdown">
            Dropdown link
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="/#">Actions</a>
            <a class="dropdown-item" href="/#">Another action</a>
            <a class="dropdown-item" href="/#">Something else here</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
    return input;
}

export default navbar;