import React from "react";

function disabledbutton({disabledbutton}){
     return <button type="button" class="btn btn-secondary btn-lg" disabled onClick={() => alert('You clicked me!')} style={{
        margin: '10px',
      }}>I am disabled</button>
}


export default disabledbutton;