import React from "react";
import Combobox from "react-widgets/Combobox";


function combobox({combobox}){
     return <Combobox
     hideCaret
     hideEmptyPopup
     data={["Red", "Yellow", "Blue", "Orange"]}
     placeholder="Search for a color"
     style={{
        margin: '10px',
      }}
   />
}


export default combobox;