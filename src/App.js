import Todo from './components/todo';
import Button from './components/button';
import DisabledButton from './components/disabledbutton';

import Inputfields from './components/inputfields';
import Combobox from './components/combobox';
import Numberpicker from './components/numberpicker';
import Navbar from './components/navbar';
import Anotherbutton from './components/anotherbutton';




import "react-widgets/styles.css";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';



function App() {
  const todos=[
    {id:1,title:"wash dishes",completed:false,},
    {id:2,title:"wash kitchen",completed:false,},
    {id:3,title:"wash dog",completed:false,},
    {id:4,title:"wash cat",completed:true,},

  ];
  return (
    <div className="App"      
    style={{
      backgroundColor: '#aec6cf',
    }}
    data-testid="background">
      <Navbar/>
      {todos.map((todo)=>{
        return (<Todo todo={todo}/>)
      })}
      <Inputfields/>
      <Combobox/>
      <Numberpicker/>
      <Button/>
      <DisabledButton/>
      <Anotherbutton/>
    </div>
  );
}

export default App;
